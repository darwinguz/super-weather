//
//  QuitoViewController.swift
//  SuperWeather
//
//  Created by Jossué Dután on 29/5/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class QuitoViewController: UIViewController {
    
    
    

    @IBOutlet weak var weatherImage: UIImageView!
    
    @IBOutlet weak var cityText: UILabel!
    @IBOutlet weak var CityLabel: UITextField!
    @IBOutlet weak var itemsTableView: UITableView!
    
    
    
    
    
    var itemManager = itemManagerJ()
    
    //el request usa otro hilo
    
    @IBAction func SaveButtonPressed(_ sender: Any) {
        
        itemManager.addItem(cityName: CityLabel.text!, itemDescription: cityText.text)
        //print(itemManager.item[0].cityName)
    }
    
    
    @IBAction func SearchButton(_ sender: Any) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(CityLabel.text ?? "quito")&appid=a8ad34488bde87c0dd278a68536dd041"
        let url = URL(string: urlString)
        let session = URLSession.shared //singleton
        let task = session.dataTask(with: url!) { (data, response, error) in
            //print (data)
            guard let data = data else{
                print("Error no data")
                return
            }
            
            guard let weatherInfo = try?JSONDecoder().decode(JossueWeatherInfo.self, from: data)
                else {
                    print("Error decoding weather")
                    return
            }
            DispatchQueue.main.async {
                self.cityText.text = "\(weatherInfo.weather[0].description)"
                let urlw = "http://openweathermap.org/img/w/\(weatherInfo.weather[0].icon).png"
                Alamofire.request(urlw).responseImage { response in
                    if let image = response.result.value {
                        self.weatherImage.image = image
                    }
                }
            }
            
        }
        task.resume()
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
