//
//  ItemManagerJ.swift
//  SuperWeather
//
//  Created by Jossué Dután on 17/7/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation
import RealmSwift

class itemManagerJ {
    var item:[ItemJ] = []
    
    var realm:Realm
    
    init(){
        realm = try! Realm()
        print(realm.configuration.fileURL)
    }
    
    func addItem(cityName:String, itemDescription:String?){
        let item = ItemJ()
        item.id = "\(UUID())"
        item.cityName = cityName
        item.itemDescription = itemDescription
        
        try! realm.write {
            realm.add(item)
        }
    }
    
    func updateArrays(){
        item = Array(realm.objects(ItemJ.self).filter("done = false"))
    }
}
