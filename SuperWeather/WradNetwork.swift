//
//  NetworkWrad.swift
//  SuperWeather
//
//  Created by Darwin Guzmán on 12/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class NetworkWrad{
    //callback (Javascript) = completion (Swift)
    func getWeather(of city:String, completion:@escaping (WradWeather)->()){
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=c4190ce87f23b52af6375650322d0358"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){ (data, response, error) in
            guard let data = data else {
                print("Error NO data")
                return
            }
            guard let weatherInfo = try? JSONDecoder().decode(WradWeatherInfo.self, from: data) else{
                print("Error decoding weather")
                return
            }
           
            completion(weatherInfo.weather[0])
            
            //DispatchQueue.main.async {
            //    self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            //}
            
            //self.resultLabel.text = "\"
        }
        task.resume()
    }
    
    func getIconWeather(iconCode:String, completionHandler: @escaping(UIImage)->()){
        let url = "http://openweathermap.org/img/w/\(iconCode).png"
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
}
